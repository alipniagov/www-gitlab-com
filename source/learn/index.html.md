---
layout: handbook-page-toc
title: "Learn@GitLab"
---

## What would you like to learn more about?
{:.no_toc}

Welcome to Learn@GitLab! It's here to provide you a deeper look into GitLab, how it works, and how it might be able to help you. It's organized by the biggest challenges we see folks come to GitLab looking to solve.

- TOC
{:toc .hidden-md .hidden-lg}

<!-- ------------------- Version Control & Collaboration ------------------- -->
## Version Control & Collaboration
<details markdown="1"><summary>show/hide this section</summary>

### Control Changes to Product Development Assets (video) 
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/l6K3Xn2MPJw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Manage, Track and Maintain Access (video) 
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/nRxCz4vMv5Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Foster Collaboration (video) 
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/OFNUjvgm2_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Merge Request Approval by API (simulation demo) 
<!-- July 2019, 12.0 -->
<a href="https://tech-marketing.gitlab.io/static-demos/mr_approval.html"><img src="/handbook/marketing/product-marketing/demo/ct-files/mr_approval.png"></a>

</details>

<!-- ------------------- Continuous Integration ------------------- -->
## Continuous Integration
<details markdown="1"><summary>show/hide this section</summary>

### Getting started with GitLab (video)
<!-- April 2020, 12.9 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/0GFEV0r_AC0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Ease of CI configuration (video) 
<!-- April 2020, 12.9 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/DGWqJotKQyM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Build and test automation (video) 
<!-- April 2020, 12.9 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/6207TKNGgJs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Visibility and Collaboration (video) 
<!-- April 2020, 12.9 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/z8r3rFQT8xg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Cross-project Pipeline Triggering and Visualization (simulation demo) 
<!-- May 2019, 11.10 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRZaaHmlJcBQS56SSS1QKyvLJQayJ31ZXqlAJMzQOMckbHt0dSj0KBN2bzg6lwny-lqfvhfhOl7tK8H/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1AVYo0HqTrwG59G2a55YzQhbXX2EKIsxQ-9P7-g_2bQc/edit?usp=sharing">G-slides file</a>

### GitLab CI with GitHub Repositories (video) 
<!-- June 2018, 11.0 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/qgl3F2j-1cI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

</details>

<!-- ------------------- Continuous Delivery ------------------- -->
## Continuous Delivery
<details markdown="1"><summary>show/hide this section</summary>

Sorry, nothing here yet.

</details>

<!-- ------------------- DevSecOps ------------------- -->
## DevSecOps
<details markdown="1"><summary>show/hide this section</summary>

### DevSecOps Use Case Overview (video) 
<!-- Feb 2020, 12.7 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/UgCHtr-6uG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Adding Security to the CICD pipeline (video) 
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Fd5DhebtScg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Managing Security Vulnerabilities with the Security Dashboard (video) 
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/t-3TSlChHy4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Dependency Scanning (video) 
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/39RvTMLDszc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Container Scanning (video) 
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/wIcaSerMfFQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### License Compliance (video) 
<!-- April 2020, 12.10 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/42f9LiP5J_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Secure Capabilities (simulation demo) 
<!-- March 2019, 11.8 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTmtpY92Uun3YsixCJHZu7yt69M9rFB5SuFRSglOBRXoFNTKBdgPZpE4JBTl3LtAAX1zS4zQdBdD6Ga/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>  
<a href="https://docs.google.com/presentation/d/1fdTmdepdaq03OSfcA3pYduDxDnQEyvY4ARPqXEX8KrY/edit#slide=id.g2823c3f9ca_0_9">G-slides file</a>

### Secure Capabilities (short, guided) (simulation demo) 
<!-- March 2019, 11.8 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQF2Neh1h0vFwMapLvhppr_bZZVaxbtnVvTP69xd6YNGreW5dZ43w4w5qQTmYNewmI-3pViilsvbIcX/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1cfzdLFWk3hYLw_aocgunVmJCD-TSiOgypr66A_nR8VQ/edit?usp=sharing">G-slides file</a>

### DevSecOps Application Security Capabilities (short, guided) (click-through demo) 
<!-- Nov 2019, 12.4 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSg77UguL0fBT75sB6aPe-DbUSiUvfcU_TIDo3MZS96dOGRkqelQBYgOz2X4rE1GHkTY8e0eVRNNrPI/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1wRL9tGPIJP8qwpLAWS05KqjR3GoqSctEJdPa0uh-Nfg/edit?usp=sharing">G-slides file</a>

</details>

<!-- ------------------- Agile Management ------------------- -->
## Agile Management
<details markdown="1"><summary>show/hide this section</summary>

<h2 class="hidden">Agile Management</h2>

### GitLab Agile Project Management 4-min Overview (video) 
<!-- Jun 2019, 11.7 -->
<figure class="video_container">
 <<iframe width="560" height="315" src="https://www.youtube.com/embed/wmtZKC8m2ew" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Agile Project Management with GitLab: 10-min Overview (video) 
<!-- Sep 2019, 11.7 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/YzlI2z_bGAo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### SAFe and Agile Planning with GitLab (video) 
<!-- Jan 2019, 11.7 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/PmFFlTH2DQk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

### VSM - Intro + Executive Insights (video) 
<!-- May 2020, 12.10 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/0MR7Q8Wiq6g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### VSM - Developer Insights (video) 
<!-- May 2020, 12.10 -->
<figure class="video_container">
 <<iframe width="560" height="315" src="https://www.youtube.com/embed/JPYWrRByAYk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### VSM - Security Insights (video) 
<!-- May 2020, 12.10 -->
<figure class="video_container">
 <<iframe width="560" height="315" src="https://www.youtube.com/embed/KSJ9VhKQAS0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### VSM - Operations Insights (video) 
<!-- May 2020, 12.10 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/lDWxH2YO3Yk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### VSM - Productivity Insights (video) 
<!-- May 2020, 12.10 -->
<figure class="video_container">
 <<iframe width="560" height="315" src="https://www.youtube.com/embed/vYKC7r8ztVA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### GitLab Agile Project Management Demo (video) 
<!-- Aug 2019, 11.7 -->
<figure class="video_container">
 <<iframe width="560" height="315" src="https://www.youtube.com/embed/cVC8bcV8zsQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### How to set up GitLab groups and projects to run multiple Agile teams with microservices (video) 
<!-- Jan 2019, 11.7 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/VR2r1TJCDew" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

### GitLab VSM - Issue Boards for Mapping (video) 
<!-- June 2018, 11.0 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/9ASHiQ2juYY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

### GitLab VSM - Business Value Monitoring (video) 
<!-- June 2018, 11.0 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/oG0VESUOFAI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

### Agile Project Management (full, guided) (simulation demo) 
<!-- June 2019, 12.0 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR5EWjzgr-Qe-cYdMj4WJnnVDxKyoyirMv0OfOJWbgCzevJoLGTXnbKDYm2TUhpdPAkh-nTcucIgZKx/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/13Zj83pjpwyq3s4T2fPSTuKO8NwqdCdn827GB7S-3hW8/edit?usp=sharing">G-slides file</a>

### Agile Project Management (short, guided) (simulation demo) 
<!-- Nov 2019, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSmsSstAwEsbh4t_vVtSIRI2-hX67H-YvQJqToE5cMH14WyENhH8xXxezU0Va9Jg79BD-SHe-kE73G-/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1gSVO07I049o0ZHDzllLX6ADgCUiuyj-icIZi_o60v80/edit?usp=sharing">G-slides file</a>

### GitLab integration with Jira (video) 
<!-- May 2018, 10.8 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/Jn-_fyra7xQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

</details>

<!-- ------------------- Simplify DevOps ------------------- -->
## Simplify DevOps
<details markdown="1"><summary>show/hide this section</summary>

### GitLab Overview - Planning to Monitoring in 12 mins (video) 
<!-- March 2020, 11.3 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/7q9Y1Cv-ib0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

### Benefits of a Single App (video) 
<!-- Aug 2019, 12.1 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/MNxkyLrA5Aw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### GitLab Planning to Monitoring (simulation demo) 
<!-- Aug 2019, 12.0 -->
<a href="https://tech-marketing.gitlab.io/static-demos/ptm-v14.html"><img src="/handbook/marketing/product-marketing/demo/ct-files/ptm.png"></a>

### GitLab Overview - Planning to Monitoring in 18 mins (video) 
<!-- Oct 2018, 11.3 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/nMAgP4WIcno" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

### Auto DevOps (full, guided) (simulation demo) 
<!-- Oct 2018, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTNeBj0TJWca1PVwccxUTXWDWYqaIyB6Q1fRYCfjtMzeK8DtpmAcG1o6ipFBi-lhYKVTAA9kYBWyKKu/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1oKHU3MsbJmxVQyO-7c6JLMoCOS80uS-0NlcI-mRxAAY/edit?usp=sharing">G-slides file</a>

### Auto DevOps (short, guided) (click-through demo) 
<!-- Oct 2018, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRQ4xltHhYRO_zgbo7exF6BwR09jvPmyFzR4XvjdlpYMRqT4dctx61XCkLjfR-8sq6QyOsoEFBBJjJh/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1UkQI_9V-CJZcbZJBDTB7tyOg14XHCKIwNoUHW1K6tC8/edit?usp=sharing">G-slides file</a>


</details>

<!-- ------------------- Cloud Native ------------------- -->
## Cloud Native
<details markdown="1"><summary>show/hide this section</summary>

### Create Kubernetes cluster (simulation demo) 
<!-- Aug 2019, 12.0 -->
<a href="https://tech-marketing.gitlab.io/static-demos/gke_k8s.html"><img src="/handbook/marketing/product-marketing/demo/ct-files/gke_setup.png"></a>

### App Modernization with Kubernetes and Serverless (short, guided) (click-through demo) 
<!-- Feb 2020, 12.7 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR9QjFBFQYAsVPOyW_afnLsRRsyysRcWqD1OKMp1GBS1JBxAomsIMsEWJuS1rff5rLCX0R4hE3XUfBx/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1Cj6qEpUUI7rOIXS150Pp34SBiZfCwWy8vFRccijnSRA/edit?usp=sharing">G-slides file</a>

### Continuous Verification with GitLab and VMware (short, guided) (click-through demo) 
<!-- Nov 2019, 12.4 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRq44Wfc19tZfTkVEfJDgSDlCNEBA0HAhYfGftgsJmYj8nHhr1KEpKiv4oE0L1Y3rM2nXnU8_tIox93/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1mYWajYWMLMBAcEcPiyh7FOMaL_0SJDo2tzeImxVFkko/edit?usp=sharing">G-slides file</a>

### Kubernetes Cluster Management an Monitoring (short, guided) (click-through demo) 
<!-- Feb 2020, 12.7 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSJSEZ8zKxeAPU0O4oYy52W48LPiTOBH1A5Hku8AtvhxbL8TrvoqJ3ehZd8i13FRDgsEKeoHkypQkts/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1zAAMsn5ZoMv6ibTLkKdZK5r9iEkFbLZzjgTtVAOHmW4/edit?usp=sharing">G-slides file</a>

### Create and use an EKS cluster using GitLab (short, guided) (click-through demo) 
<!-- Dec 2019, 12.5 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQlDO-z2RHvElzBYnfFonJsxwh0M1YeU1bPl7yXduWtuj55L-DRSf9P6-h6zk4v7yBgA1nVNLxdJPth/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1iXnB6lvTx2_-_0ASElLUDZwyFPWILCRx54XjJkMFuw0/edit?usp=sharing">G-slides file</a>

</details>


<!-- ------------------- GitOps ------------------- -->
## GitOps
<details markdown="1"><summary>show/hide this section</summary>

### GitOps with GitLab (short, guided) (click-through demo) 
<!-- Dec 2019, 12.5 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQTBQAHx7zyNd4o3YIyKmFEsRJl8-BCdd2g6MdCKuJuFab_HNea_HYK7HDSzd3macx6LnVtYwIlCxV7/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1UT32lLvXtwAslkK7o8asbko3a231WKrjmlcM0z9coPw/edit?usp=sharing">G-slides file</a>

### GitOps Process with GitLab (video) 
<!-- Oct 2019, 12.3 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/wk7YAXijIZI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### GitOps Infrastructure with GitLab (video) 
<!-- Nov 2019, 12.4 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/5rqoLj8N5PA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### GitOps Applications with GitLab (video) 
<!-- Nov 2019, 12.4 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/heQ1WY_08Tc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

</details>


<!-- ------------------- Remote Development ------------------- -->
## Remote Development
<details markdown="1"><summary>show/hide this section</summary>

### GitLab for Remote Teams (video) 
<!-- April 2020, 12.10 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/qCDAioq3eis" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

</details>


<!-- ------------------- Other Use Cases and Overviews ------------------- -->
## Other Use Cases and Overviews
<details markdown="1"><summary>show/hide this section</summary>

### GitLab in 3 minutes 
<!-- Aug 2019, 12.1 -->
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Jve98tlZ394" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

</details>


<!-- ------------------- Archive ------------------- -->
## Archive
<details markdown="1"><summary>show/hide this section</summary>

### Auto DevOps - Setup (GKE) 
<!-- Oct 2018, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQIPrinsvTG1s6ppnUWqSY-fpHnxe6oAwM7g91uBl8Mx3EYQhaejlKUF9_c3GtagIhzwg-8dJlIrNgw/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1AGABPlNzMm5-rrYfwGIzueXIbPleVkGpnc2Qk6JtnWk/edit?usp=sharing">G-slides file</a>

### Auto DevOps - Setup (EKS) 
<!-- Oct 2018, 11.3 -->
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRjhO2VvKf_gWbObwi5APm18gxQrzQk5vvAARD-4vLQeT0NbkrSuP3t4sTVylRYZOD6kINLr37HmtHA/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<a href="https://docs.google.com/presentation/d/1Ejnho9pqXPj-OHNU2q51cC0xCG5c8pVLmvg-maIA7BQ/edit?usp=sharing">G-slides file</a>

### Auto DevOps in GitLab 11.0 
<!-- June 2018, 11.0 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/0Tc0YYBxqi4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

### 2018 Vision Prototype 
<!-- Feb 2018, 10.5 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/RmSTLGnEmpQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

### GitLab for the Enterprise 
<!-- Feb 2018, 10.5 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/gcWfUw_Cau4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

### GitLab CI/CD Deep Dive Live Demo 
<!-- July 2017, 9.4 -->
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/pBe4t1CD8Fc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

</details>

